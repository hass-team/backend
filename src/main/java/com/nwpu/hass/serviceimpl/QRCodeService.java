package com.nwpu.hass.serviceimpl;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.utils.qrcodeutils.Img2PdfImg;
import com.nwpu.hass.utils.qrcodeutils.QRCodeUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <h3>hass</h3>
 * <p>生成给二维码并转换为pdf的服务</p>
 *
 * @author : Qin
 * @date : 2021-01-13 14:58
 **/
public class QRCodeService {
    private static final String DESTINATION = "/barcodeimgs/barcodes.pdf";

    public static File generateQRCodePdf(DeviceEntity[] deviceEntities) throws Exception {
        List<File> fileList = new ArrayList<>();
        // 生成设备的条形码
        for(DeviceEntity deviceEntity:deviceEntities){
            String path = "./barcodeimgs/"+deviceEntity.getDeviceId()+".jpg";
            String type = "type:" + deviceEntity.getType();
            String family = "family:" + deviceEntity.getFamily();
            String deviceName = "deviceName:"+deviceEntity.getDeviceName();
            String id = "deviceId:" + deviceEntity.getDeviceId();
            String content = type + family +deviceName + id ;
            File result = QRCodeUtil.toQRCode(content,path,id);
            fileList.add(result);
        }
        // 将多张图片转换为一个pdf文件
        File[] files = new File[fileList.size()];
        for(int i= 0 ; i <fileList.size();i++){
            files[i] = fileList.get(i);
        }
        String projectPath = new File("").getCanonicalPath();
        Img2PdfImg.imagesToPdf(projectPath+DESTINATION,files);
        for(File file:fileList){
            file.delete();
        }
        return new File(projectPath + DESTINATION);
    }
}
