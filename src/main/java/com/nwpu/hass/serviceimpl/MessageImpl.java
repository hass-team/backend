package com.nwpu.hass.serviceimpl;

import com.nwpu.hass.domain.MessageEntity;
import com.nwpu.hass.domain.SystemMessageEntity;
import com.nwpu.hass.repository.MessageRepository;
import com.nwpu.hass.repository.SystemMessageRepository;
import com.nwpu.hass.service.MessageService;
import com.nwpu.hass.utils.Tools;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MessageImpl implements MessageService {

    @Resource
    MessageRepository messageRepository;
    @Resource
    SystemMessageRepository systemMessageRepository;

    @Override
    public List<MessageEntity> getMessageList(long userId, long pageNo) {

        return messageRepository.findAllByUserId(userId, (pageNo - 1) * 7, 7);
    }

    @Override
    public MessageEntity addMessage(long userId, String msg) {

        MessageEntity messageEntity = new MessageEntity();
        messageEntity.setContent(msg);
        messageEntity.setRecord(Tools.timestamp());
        messageEntity.setUserid(userId);
        return messageRepository.save(messageEntity);
    }

    @Override
    public List<SystemMessageEntity> getSystemMessageList(long pageNo) {

        return systemMessageRepository.findAllMessage( (pageNo - 1) * 7, 7);

    }

    @Override
    public SystemMessageEntity addSystemMessage(String msg) {

        SystemMessageEntity systemMessageEntity = new SystemMessageEntity();
        systemMessageEntity.setContent(msg);
        systemMessageEntity.setRecord(Tools.timestamp());
        return systemMessageRepository.save(systemMessageEntity);
    }

}
