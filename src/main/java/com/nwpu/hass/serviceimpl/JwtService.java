package com.nwpu.hass.serviceimpl;

import com.auth0.jwt.interfaces.Claim;
import com.nwpu.hass.domain.AuthEntity;
import com.nwpu.hass.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

/**
 * @author alecHe
 * @desc 提供的服务
 *      1.JWT的签发服务
 *      2.JWT的验证服务
 *      3.JWT的更新服务
 * @date 2021-01-09 11:05:56
 */
@Service
@PropertySource("classpath:application.yml")
public class JwtService {

    @Value("${jwt_expire_time}")
    private static long expireTime;

    public static void setExpireTime(long expireTime) {
        JwtService.expireTime = expireTime * 60 * 1000;
    }

    /**
     * 签发JWT
     * @param authEntity payload信息
     * @return 已签发的JWT
     *
     */
    public String generateNewJwt(AuthEntity authEntity,String secret){
        return JwtUtils.encodeJwt(authEntity,secret,expireTime);
    }

    /**
     * 验证JWT有效性：虽然存在Jwt，但secret可能出错导致无法解密
     * @param token JWT
     * @param secret 密钥
     * @return 是否通过
     */
    public boolean verifyJWT(String token,String secret){
        try {
            JwtUtils.decodeJwt(token,secret);
            return true;
        }
        catch (Exception e){
            //若验证不通过或token为空，将直接抛出异常
            return false;
        }
    }

//    /**
//     * 过期时间小于半小时，返回新的jwt，否则返回原jwt
//     * @param jwt
//     * @return
//     */
//    public String refreshJwt(String jwt){
//        String secret = RedisUtil.redisTemplate.opsForValue().get(jwt);
//        Map<String, Claim> map = JwtUtil.decode(jwt,secret);
//        if(map.get("exp").asLong()*1000 - System.currentTimeMillis()/1000<30*60*1000){
//            return this.generateNewJwt(map.get("name").asString());
//        }else{
//            return jwt;
//        }
//    }


}
