package com.nwpu.hass.serviceimpl;

/**
 * @author alecHe
 * @desc Indentity center 认证中心
 *      主要提供token的签发服务
 * @date 2021-01-09 13:36:21
 */

import com.nwpu.hass.domain.AuthEntity;
import com.nwpu.hass.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.transform.sax.SAXResult;
import java.util.UUID;
@Slf4j
@Service
public class IdentifyService {

    @Autowired
    private RedisService redisService;

    @Autowired
    private JwtService jwtService;

    public String signJwt(AuthEntity authEntity){
        String secret = UUID.randomUUID().toString().replaceAll("-","");
        String token = jwtService.generateNewJwt(authEntity,secret);
        redisService.set(token,secret);
        log.info("Jwt已签发");
        return token;
    }
}
