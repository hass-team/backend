package com.nwpu.hass.serviceimpl;

import com.nwpu.hass.utils.faceutils.GsonUtils;
import com.nwpu.hass.utils.faceutils.HttpUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * <h3>hass</h3>
 * <p>人脸识别相关的服务</p>
 *
 * @author : Qin
 * @date : 2021-01-10 19:17
 **/
@Slf4j
public class FaceService {
    public static String add(String image,Long userId,String token) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/face/v3/faceset/user/add";
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("image", image);   // 图片信息，BASE64编码之后的字符串
            map.put("group_id", "userPic");
            map.put("user_id", userId);
            /*
             * FACE_TOKEN 人脸图片的唯一标识，调用人脸检测接口时，会为每个人脸图片赋予一个唯一的
             *            FACE_TOKEN,同一张图片多次检测到的FACE_TOKEN是同一个
             * BASE64     图片的base64,base64编码后的图片数据，编码后的图片大小不能超过2M
             */
            map.put("image_type", "BASE64");
            map.put("quality_control", "NORMAL");

            String param = GsonUtils.toJson(map);
            String result = HttpUtil.post(url, token, "application/json", param);
            System.out.println(result);
            return result;
        } catch (Exception e) {

        }
        return null;
    }

    public static String faceMatch(String image,String token) {
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/face/v3/search";
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("image", image);   // 图片信息，BASE64编码之后的字符串
            map.put("group_id_list", "userPic");
            map.put("image_type", "BASE64");
            map.put("quality_control", "NORMAL");
            String param = GsonUtils.toJson(map);
            log.info(param);
            String result = HttpUtil.post(url, token, "application/json", param);
            System.out.println(result);
            return result;
        } catch (Exception e) {

        }
        return null;
    }
}
