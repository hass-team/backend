package com.nwpu.hass.service;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.JsonResponse.ApplyEntity;
import com.nwpu.hass.domain.JsonResponse.DevicesByDepartmentResponse;
import com.nwpu.hass.domain.JsonResponse.DevicesByStatusResponse;
import org.springframework.web.multipart.MultipartFile;
import com.nwpu.hass.domain.JsonResponse.IdleDevicesGroupResponse;

import java.util.List;

/**
 * 设备操作
 *
 * @author wjy
 * @version 1.1.0
 */
public interface DeviceService {

    /**
     * 借出一台指定type和family的设备，多个符合要求时借出最早归还的一台，没有空闲设备时返回null
     *
     * @param type 设备类型
     * @param family 设备家族
     * @param userId 请求借用设备的用户
     * @param deviceName 请求借用的设备名
     * @return 符合要求的一台设备
     */
    DeviceEntity idleDevice(String type, String family, String deviceName, Long userId);

    /**
     * 归还id为returnId的设备
     *
     * @param returnId 归还设备id
     * @return 归还的设备
     */
    DeviceEntity returnDevice(Long userId,Long returnId);

    /**
     * 归还申请列表
     *
     * @return 归还申请列表
     */
    List<ApplyEntity> useChecksReturnList(Integer pageNo);

    /**
     * 借出申请列表
     *
     * @return 借出申请列表
     */
    List<ApplyEntity> useChecksBorrowList(Integer pageNo);

    /**
     * 更新设备信息
     * @param deviceEntity 设备实体类
     */
    void updateDevice(DeviceEntity deviceEntity);

    /**
     * 通过设备id获得DeviceEntity
     * @param deviceId 设备id
     * @return 返回DeviceEntity
     */
    DeviceEntity getDeviceById(Long deviceId);

    List<DeviceEntity> getAllDevice(int pageNo);

    /**
     * 入库Excel表中所有设备
     *
     * @param userId 仓库管理员id
     * @param file excel文件
     * @return 添加后设备列表
     */
    List<DeviceEntity> checkInDevices(long userId, MultipartFile file);

    /**
     * 获取所有空闲的设备，根据type,family,deviceName分组,统计每组的Count
     * @return
     */
    List<IdleDevicesGroupResponse> getAllIdleDeviceGroup(int min,int max);

    /**
     * 查询用户已借的所有设备列表
     *
     */
    List<DeviceEntity> getAllBorrowedDevice(Long userId,Integer pageNo);

    /**
     * 获取一条设备详情信息
     *
     * @param deviceId 设备ID
     * @return 指定id的设备实体
     */
    DeviceEntity getOneDeviceInfo(long deviceId);

    /**
     * 获取所有没有位置的设备
     *
     * @return 没有位置设备列表
     */
    List<DeviceEntity> getNoPosDevices(int pageNo);

    /**
     * 直接调用即可将报修信息通过邮件、微信的方式发送给所有仓库管理员
     *
     */
    void sendMailAndWeChat(String deviceIds) throws Exception;


    List<DevicesByStatusResponse> getDeviceGroupByStatus();

    List<DevicesByDepartmentResponse> getDeviceGroupByDepartment();

    List<DeviceEntity> getOnDevices(Long userId,int status);

    List<DeviceEntity> getDevicesByFamily(String family);

    List<DeviceEntity> getDevicesByStatus(int status);

    List<DeviceEntity> getDevicesByStatusPageNo(int pageNo);
}
