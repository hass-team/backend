package com.nwpu.hass.service;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.JsonResponse.ApplyEntity;
import com.nwpu.hass.domain.JsonResponse.LastFourDayLentResponse;
import com.nwpu.hass.domain.JsonResponse.UsedDeviceFreqResponse;

import java.util.List;
import java.util.Optional;

/**
 * 借出归还记录操作
 *
 * @author wjy
 * @version v1.0.0
 */
public interface RegisterformService {

    /**
     * 通过记录id查找一条借出/归还记录
     *
     * @param recordId 记录id
     * @return 记录
     */
    ApplyEntity findOneRegisterById(Long recordId);

    /**
     * 审核一条借出归还申请
     *
     * @param id 申请记录id
     * @param status 是否通过，true为通过，false不通过
     * @return 审核后的设备
     */
    DeviceEntity checkOneRegisterById(Long id, boolean status);

    List<UsedDeviceFreqResponse> getDeviceByFreq();

    List<LastFourDayLentResponse> getLastFourLentDevice();
}
