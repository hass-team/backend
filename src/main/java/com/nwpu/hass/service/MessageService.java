package com.nwpu.hass.service;

import com.nwpu.hass.domain.MessageEntity;
import com.nwpu.hass.domain.SystemMessageEntity;

import java.util.List;

/**
 * 消息服务
 */
public interface MessageService {

    /**
     * 获取用户指定页码的消息列表
     *
     * @param userId 用户id
     * @param pageNo 请求页码，每页7条
     * @return 消息列表
     */
    List<MessageEntity> getMessageList(long userId, long pageNo);

    /**
     * 为指定用户添加一条消息
     *
     * @param userId 用户id
     * @param msg 消息内容
     * @return 添加的消息实体
     */
    MessageEntity addMessage(long userId, String msg);

    /**
     * 获取指定页码的系统消息列表
     *
     * @param pageNo 请求页码，每页7条
     * @return 系统消息列表
     */
    List<SystemMessageEntity> getSystemMessageList(long pageNo);

    /**
     * 添加一条系统消息
     *
     * @param msg 消息内容
     * @return 添加的消息实体
     */
    SystemMessageEntity addSystemMessage(String msg);
}
