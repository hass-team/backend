package com.nwpu.hass.service;


import com.nwpu.hass.domain.UserEntity;
import java.util.*;

public interface UserService {

    /**
     * 添加一个新的UserEntity到数据库中
     * @param userEntity 新的UserEntity
     */
    Long addUser(UserEntity userEntity);

    /**
     * 通过用户id查找相应的用户
     * @param userId 用户id
     * @return 相应的用户;如果不存在该id对应的用户，则是null
     */
    UserEntity getUserById(Long userId);

    /**
     * 通过用户username查找相应的用户
     * @param username 用户名
     * @return 相应的用户;如果不存在该username对应的用户，则是null
     */
    UserEntity getUserByUsername(String username);

    /**
     * 更改用户信息
     * @param userEntity 修改后的用户
     */
    void modifiedUser(UserEntity userEntity);

    /**
     * 删除用户
     * @param userEntity 要删除的用户
     */
    boolean deleteUser(UserEntity userEntity);

    /**
     * 获取所有用户
     * @return 所有用户信息
     */
    List<UserEntity> getAllUser(int min,int max);

    /**
     * 向微信服务器发送openId申请
     * @param code 请求code
     * @return 用户的openId
     */
    String getOpenId(String code) throws Exception;

}
