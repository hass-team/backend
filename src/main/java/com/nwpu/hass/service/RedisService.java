package com.nwpu.hass.service;

import com.nwpu.hass.domain.AuthEntity;

public interface RedisService {

    Boolean verifyToken(String token);

    /**
     * 将token-secret信息存储至Redis
     * @param token 签发的JWT
     * @param secret 密钥
     */
    void set(String token, String secret);

    AuthEntity getAuth (String token);
    /**
     * 从Redis中获取userId
     * @param token 签发JWT
     * @return 用户id
     */
    Long getUserId(String token);
}
