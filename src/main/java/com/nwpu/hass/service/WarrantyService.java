package com.nwpu.hass.service;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.JsonResponse.RepairApplyResponse;
import com.nwpu.hass.domain.WarrantyEntity;
import com.nwpu.hass.utils.Code;
import java.util.*;

/**
 * 报修操作
 *
 * @version 1.0.0
 * @author  wjy
 */
public interface WarrantyService {

    /**
     * 申请报修
     *
     * @param deviceId 设备id
     * @param reason 维修原因
     * @param userId 申请者id
     * @return 返回保修结果
     */
    String appendRepair(long deviceId, String reason, long userId);

    /**
     * 根据记录id查找一条报修申请
     *
     * @param recordId 报修申请id
     * @return 一条报修请求
     */
    RepairApplyResponse getOneRepairRecord(long recordId);

    /**
     * 审核保修申请，如果审核通过，将设备状态置为报修中
     *              如果审核不通过，将设备状态改为空闲
     *
     * @param id 记录id
     * @param status 是否通过
     * @return 返回操作结果的描述
     */
     String checkOneById(Long id, boolean status);

     List<RepairApplyResponse> getCheckList(int pageNo);
}
