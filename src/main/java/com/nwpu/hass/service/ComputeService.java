package com.nwpu.hass.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-11 11:53:32
 */
public interface ComputeService extends Remote {

    Double serviceOffer(String keyword,String catagoryName, String brand, String type)throws RemoteException;
}
