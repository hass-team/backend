package com.nwpu.hass.interceptor;

import com.nwpu.hass.domain.AuthEntity;
import com.nwpu.hass.service.RedisService;
import com.nwpu.hass.utils.Code;
import com.nwpu.hass.utils.JwtUtils;
import com.nwpu.hass.utils.MyResponseEntity;
import com.nwpu.hass.utils.Tools;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;
import java.util.Objects;

/**
 * @author alecHe
 * @desc 第二步拦截，即通过了redis验证，当前token有效
 *       判断用户是否有权限访问响应的接口
 * @date 2021-01-09 12:28:07
 */
@Slf4j
@Component
@Aspect
public class Role_Interceptor {

	@Autowired
	private RedisService redisService;

	Integer usualUser = 0;
	Integer managerUser = 2;
	Integer adminUser = 3;

	/**
	 * 只有仓库管理员能够调用的方法,处理一条申请借出和申请归还的记录
	 * 							  数据可视化的接口
	 * 							  处理报废申请
	 * 							  处理报修申请
	 * 							  修改设备信息
	 */
	@Pointcut("execution(* com.nwpu.hass.controller.DeviceController.processApply(..)) &&" +
			"execution(* com.nwpu.hass.controller.DataController.*())&&" +
			"execution(* com.nwpu.hass.controller.DeviceController.handleScrap())&&" +
			"execution(* com.nwpu.hass.controller.UserController.deleteUser(..))&&" +
			"execution(* com.nwpu.hass.controller.WarrantyController.processRepairRecord())&&" +
			"execution(* com.nwpu.hass.controller.DeviceController.modifyInfo(..))")
	public void managerAccess() {}

	@Around("managerAccess()")
	public MyResponseEntity<Object> managerHandler(ProceedingJoinPoint joinPoint) throws Throwable {

		//获取token，其中token存放在header中
		Map<String,String> queryParams = Tools.getQueryParameters();
		String token = queryParams.get("token");

		AuthEntity authEntity = redisService.getAuth(token);
		if (authEntity.getRole() != managerUser) {
			log.info("用户权限不够，当前是仓库管理员功能");
			return new MyResponseEntity<>(Code.INVALID_TOKEN, null);
		}

		Object result = joinPoint.proceed();

		return (MyResponseEntity<Object>) result;
	}

	/**
	 *  只有系统管理员才能调用的方法,删除用户,修改用户权限,获取用户列表
	 */
	@Pointcut("execution(* com.nwpu.hass.controller.UserController.deleteUser(..))&&" +
			"execution(* com.nwpu.hass.controller.UserController.modifyUserPerssion())&&" +
			"execution(* com.nwpu.hass.controller.UserController.getUserList())")
	public void adminAccess() {}

	@Around("adminAccess()")
	public MyResponseEntity<Object> adminHandler(ProceedingJoinPoint joinPoint) throws Throwable {

		//获取token，其中token存放在header中
		Map<String,String> queryParams = Tools.getQueryParameters();
		String token = queryParams.get("token");

		AuthEntity authEntity = redisService.getAuth(token);
		if (authEntity.getRole()!=adminUser) {
			log.info("用户权限不够，当前是系统管理员功能");
			return new MyResponseEntity<>(Code.INVALID_TOKEN, null);
		}

		Object result = joinPoint.proceed();

		return (MyResponseEntity<Object>) result;
	}

}
