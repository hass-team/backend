package com.nwpu.hass.exceptions;


import com.nwpu.hass.utils.Code;

public class NoUserFoundException extends RuntimeException {
	private int code;
	private String msg;

	public NoUserFoundException() {
		this(Code.BAD_OPERATION.getCode(), Code.BAD_OPERATION.getMsg());
	}

	public NoUserFoundException(String msg) {
		this(Code.BAD_OPERATION.getCode(), msg);
	}

	public NoUserFoundException(int code, String msg) {
		super(msg);
		this.code = code;
		this.msg = msg;
	}
}
