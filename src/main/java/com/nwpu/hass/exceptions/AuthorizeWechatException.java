package com.nwpu.hass.exceptions;

import com.nwpu.hass.utils.Code;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-13 14:29:48
 */
public class AuthorizeWechatException extends RuntimeException{
    private int code;
    private String msg;

    public AuthorizeWechatException() {
        this(Code.INVALID_TOKEN.getCode(), Code.INVALID_TOKEN.getMsg());
    }

    public AuthorizeWechatException(String msg) {
        this(Code.INVALID_TOKEN.getCode(), msg);
    }

    public AuthorizeWechatException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
