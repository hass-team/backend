package com.nwpu.hass.exceptions;

import com.nwpu.hass.utils.Code;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-09 11:01:15
 */
public class AuthorizeException extends RuntimeException{
    private int code;
    private String msg;

    public AuthorizeException() {
        this(Code.INVALID_TOKEN.getCode(), Code.INVALID_TOKEN.getMsg());
    }

    public AuthorizeException(String msg) {
        this(Code.INVALID_TOKEN.getCode(), msg);
    }

    public AuthorizeException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
