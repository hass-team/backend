package com.nwpu.hass.repository;

import com.nwpu.hass.domain.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, Long> {

    @Query(nativeQuery = true, value = "select * from message where userid = ?1 order by record DESC limit ?2, ?3")
    List<MessageEntity> findAllByUserId(long userId, long offset, long pageSize);

    void deleteMessageEntitiesByUserid(long userId);

}
