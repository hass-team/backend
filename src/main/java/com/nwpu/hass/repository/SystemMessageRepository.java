package com.nwpu.hass.repository;

import com.nwpu.hass.domain.SystemMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface SystemMessageRepository extends JpaRepository<SystemMessageEntity, Long> {

    @Query(nativeQuery = true, value = "select * from systemMessage order by record DESC limit ?1, ?2")
    List<SystemMessageEntity> findAllMessage(long offset, long pageSize);

}
