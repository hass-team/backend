package com.nwpu.hass.repository;

import com.nwpu.hass.domain.WarrantyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WarrantyRepository extends JpaRepository<WarrantyEntity, Long> {

    List<WarrantyEntity> findAllByDeviceOrderByRecordDesc(Long deviceId);

    List<WarrantyEntity> findAllByApplyPersonOrderByRecordDesc(Long applyPerson);

    void deleteWarrantyEntitiesByApplyPerson(Long applyPerson);
}
