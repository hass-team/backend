package com.nwpu.hass.repository;

import com.nwpu.hass.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-06 15:32:10
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long> {

    /**
     * 根据用户id查找UserEntity
     * @param id 用户id
     * @return  对应的用户实体类
     */
    UserEntity findUserEntityByUserid(Long id);

    /**
     * 根据用户名查找UserEntity
     * @param username 用户名
     * @return 对应的用户实体类
     */
    UserEntity findUserEntityByUsername(String username);

    /**
     * 通过角色查找用户
     * @param mrole 用户角色
     * @return 属于该角色的用户列表
     */
    List<UserEntity> findUserEntityByMrole(int mrole);

    @Query(nativeQuery = true, value = "select * from user limit ?1, ?2")
    List<UserEntity> findAllUser(int min,int max);

}
