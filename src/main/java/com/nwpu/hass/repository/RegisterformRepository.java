package com.nwpu.hass.repository;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.RegisterformEntity;
import org.apache.xmlbeans.impl.xb.xsdschema.LocalSimpleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 借出、归还记录的持久化
 *
 * @author wjy
 * @version v1.0.0
 */
@Repository
public interface RegisterformRepository extends JpaRepository<RegisterformEntity, Long> {

    @Query(nativeQuery = true, value = "select * from registerform where device = ?1 order by record DESC")
    List<RegisterformEntity> findByDeviceId(Long deviceId);

    RegisterformEntity findRegisterformEntityByBorrowPersonAndDeviceOrderByRecordDesc(Long userId,Long deviceId);

    /**
     * 查找某人的所有借出归还申请记录，通过记录时间排序
     *
     * @param borrower 借用人
     * @return 记录列表
     */
    List<RegisterformEntity> findAllByBorrowPersonOrderByRecordDesc(Long borrower);

    /**
     * 查找某个设备的的所有借出归还申请记录，通过记录时间排序
     * @param device 设备
     * @return 记录列表
     */
    List<RegisterformEntity> findAllByDeviceOrderByRecordDesc(Long device);

    void deleteRegisterformEntitiesByBorrowPerson(Long borrowPerson);

    /**
     * 获取使用前10的设备
     * @return
     */
    @Query(nativeQuery = true, value = "select * " +
            "from device " +
            "where status = -3")
    Optional<DeviceEntity> getDeviceByFreq();

}
