package com.nwpu.hass;

import com.thebeastshop.forest.springboot.annotation.ForestScan;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@ForestScan(basePackages = "com.nwpu.hass.utils")
@EnableAsync
@SpringBootApplication(exclude = {MybatisAutoConfiguration.class})
public class HassApplication {

    public static void main(String[] args) {
        SpringApplication.run(HassApplication.class, args);
    }

}
