package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class AssignPosition {

    @NotNull
    long deviceId;

    @Size(min = 2, max = 20)
    @NotNull
    String position;
}
