package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * <h3>hass</h3>
 * <p> 修改设备信息</p>
 *
 * @author : Qin
 * @date : 2021-01-07 14:25
 **/
@Data
public class ModifyInfoRequestBody {

    @NotNull(message = "设备Id不能为空")
    private long deviceId;

    @NotNull(message = "设备名称不能为空")
    private String deviceName;

    @NotNull(message = "设备类型不能为空")
    private String type;

    @NotNull(message = "设备不能为空")
    private String family;

    private Date purchasedTime;

    private int usedTimes;

    @NotNull(message = "设备状态不能为空")
    private int status;

    private double cost;

    private int usedYear;

    private String devicePic;

    private String position;

}
