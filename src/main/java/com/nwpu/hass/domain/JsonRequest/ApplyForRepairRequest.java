package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ApplyForRepairRequest {

    @NotNull
    Long deviceId;

    @Size(min = 5, max = 60)
    @NotNull
    String repairReason;
}
