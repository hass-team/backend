package com.nwpu.hass.domain.JsonRequest;



import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;


/**
 * @author AaronQin
 * @desc 注册用户的信息类
 * @date 2021-01-06 23:41
 */
@Data
public class UserRegisterRequestBody {
    @NotNull(message = "用户名不能为空")
    @Size(min = 2, max = 20, message = "账号长度必须是2-20个字符")
    private String username;

    @NotNull(message = "用户密码不能为空")
    @Size(min = 6, max = 20, message = "密码长度必须是6-20个字符")
    private String mpassword;

    @NotNull(message = "电话不能为空")
    @Size(min = 7, max = 11, message = "电话的长度必须是7-11位数字")
    private String phone;

    @NotNull(message = "用户邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    private String email;

    @NotNull(message = "部门不能为空")
    private String depwork;

    @NotNull(message = "姓名不能为空")
    @Size(min = 2, max = 20, message = "姓名的长度必须是2-20位字符")
    private String fullname;

    private String wechatAccount;
}
