package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h3>hass</h3>
 * <p>用户登录请求类</p>
 *
 * @author : Qin
 * @date : 2021-01-07 10:26
 **/
@Data
public class UserLoginRequestBody {
    @NotNull(message = "用户名不能为空")
    @Size(min = 2, max = 20, message = "账号长度必须是6-20个字符")
    private String username;

    @NotNull(message = "用户密码不能为空")
    @Size(min = 6, max = 20, message = "密码长度必须是6-20个字符")
    private String mpassword;

}
