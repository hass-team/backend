package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 审核某一条借出、归还申请的请求体
 */
@Data
public class ProcessApplyRequest {

    //申请记录id
    @NotNull
    private long id;

    //通过
    @NotNull
    private boolean pass;
}
