package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <h3>hass</h3>
 * <p>处理报废请求消息</p>
 *
 * @author : Qin
 * @date : 2021-01-07 16:34
 **/
@Data
public class HandleScrapRequestBody {
    @NotNull(message="审核结果不能为空")
    private boolean pass;

    @NotNull(message = "设备id不能为空")
    private long deviceId;
}
