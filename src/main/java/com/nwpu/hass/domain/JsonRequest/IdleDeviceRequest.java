package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class IdleDeviceRequest {
    @NotNull
    String type;
    @NotNull
    String family;
    @NotNull
    String deviceName;
}
