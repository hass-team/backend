package com.nwpu.hass.domain.JsonRequest;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h3>hass</h3>
 * <p>修改用户权限请求</p>
 *
 * @author : Qin
 * @date : 2021-01-07 11:48
 **/
@Data
public class ModifidyPermissionRequestBody {
    @NotNull(message = "用户id不能为空")
    private long userId;

    @NotNull(message = "用户密码不能为空")
    private int modifiedPermission;
}
