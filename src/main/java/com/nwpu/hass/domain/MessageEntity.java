package com.nwpu.hass.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "message", schema = "hass", catalog = "")
public class MessageEntity {
    private int messageId;
    private String content;
    private Timestamp record;
    private Long userid;
    private UserEntity userByUserid;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "messageId", nullable = false)
    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "record", nullable = false)
    public Timestamp getRecord() {
        return record;
    }

    public void setRecord(Timestamp record) {
        this.record = record;
    }

    @Basic
    @Column(name = "userid", nullable = true)
    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageEntity that = (MessageEntity) o;
        return messageId == that.messageId && Objects.equals(content, that.content) && Objects.equals(record, that.record) && Objects.equals(userid, that.userid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageId, content, record, userid);
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "userid", referencedColumnName = "userid",insertable = false,updatable = false)
    public UserEntity getUserByUserid() {
        return userByUserid;
    }

    public void setUserByUserid(UserEntity userByUserid) {
        this.userByUserid = userByUserid;
    }
}
