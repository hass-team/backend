package com.nwpu.hass.domain;

import lombok.Data;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-13 14:23:47
 */
@Data
public class Auth4Wechat {
    private String session_key;
    private String openid;
}
