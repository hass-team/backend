package com.nwpu.hass.domain;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-12 13:10:46
 */
@Entity
@Table(name = "view4keyword", schema = "hass", catalog = "")
public class View4KeywordEntity {
    private String family;
    private String type;
    private String deviceName;
    private long count;
    private String id;

    @Id
    @Basic
    @Column(name = "family", nullable = false, length = 255)
    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 255)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "deviceName", nullable = true, length = 255)
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Basic
    @Column(name = "count(*)", nullable = false)
    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        View4KeywordEntity that = (View4KeywordEntity) o;
        return count == that.count &&
                Objects.equals(family, that.family) &&
                Objects.equals(type, that.type) &&
                Objects.equals(deviceName, that.deviceName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(family, type, deviceName, count);
    }

}
