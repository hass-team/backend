package com.nwpu.hass.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

/**
 * <h3>hass</h3>
 * <p></p>
 *
 * @author : Qin
 * @date : 2021-01-08 03:31
 **/
@Entity
@Table(name = "device", schema = "hass", catalog = "")
public class DeviceEntity {
    private long deviceId;
    private String type;
    private String family;
    private String deviceName;
    private Integer usedYear;
    private Timestamp purchasedTime;
    private Double cost;
    private Integer status;
    private Integer usedTimes;
    private String devicePic;
    private Timestamp record;
    private String position;
    private Collection<RegisterformEntity> registerformsByDeviceId;
    private Collection<WarrantyEntity> warrantiesByDeviceId;

    @Id
    @Column(name = "deviceId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 255)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "family", nullable = false, length = 255)
    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    @Basic
    @Column(name = "deviceName", nullable = true, length = 255)
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Basic
    @Column(name = "usedYear", nullable = true)
    public Integer getUsedYear() {
        return usedYear;
    }

    public void setUsedYear(Integer usedYear) {
        this.usedYear = usedYear;
    }

    @Basic
    @Column(name = "purchasedTime", nullable = true)
    public Timestamp getPurchasedTime() {
        return purchasedTime;
    }

    public void setPurchasedTime(Timestamp purchasedTime) {
        this.purchasedTime = purchasedTime;
    }

    @Basic
    @Column(name = "cost", nullable = true, precision = 0)
    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "usedTimes", nullable = true)
    public Integer getUsedTimes() {
        return usedTimes;
    }

    public void setUsedTimes(Integer usedTimes) {
        this.usedTimes = usedTimes;
    }

    @Basic
    @Column(name = "devicePic", nullable = true, length = 255)
    public String getDevicePic() {
        return devicePic;
    }

    public void setDevicePic(String devicePic) {
        this.devicePic = devicePic;
    }

    @Basic
    @Column(name = "record", nullable = false)
    public Timestamp getRecord() {
        return record;
    }

    public void setRecord(Timestamp record) {
        this.record = record;
    }

    @Basic
    @Column(name = "position", nullable = true, length = 255)
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceEntity that = (DeviceEntity) o;
        return deviceId == that.deviceId &&
                Objects.equals(type, that.type) &&
                Objects.equals(family, that.family) &&
                Objects.equals(deviceName, that.deviceName) &&
                Objects.equals(usedYear, that.usedYear) &&
                Objects.equals(purchasedTime, that.purchasedTime) &&
                Objects.equals(cost, that.cost) &&
                Objects.equals(status, that.status) &&
                Objects.equals(usedTimes, that.usedTimes) &&
                Objects.equals(devicePic, that.devicePic) &&
                Objects.equals(record, that.record) &&
                Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, type, family, deviceName, usedYear, purchasedTime, cost, status, usedTimes, devicePic, record, position);
    }

    @OneToMany(mappedBy = "deviceByDevice")
    @JsonIgnore
    public Collection<RegisterformEntity> getRegisterformsByDeviceId() {
        return registerformsByDeviceId;
    }

    public void setRegisterformsByDeviceId(Collection<RegisterformEntity> registerformsByDeviceId) {
        this.registerformsByDeviceId = registerformsByDeviceId;
    }

    @OneToMany(mappedBy = "deviceByDevice")
    @JsonIgnore
    public Collection<WarrantyEntity> getWarrantiesByDeviceId() {
        return warrantiesByDeviceId;
    }

    public void setWarrantiesByDeviceId(Collection<WarrantyEntity> warrantiesByDeviceId) {
        this.warrantiesByDeviceId = warrantiesByDeviceId;
    }

    /**
     * 审核通过或拒绝借出归还申请
     *
     * @param pass 申请或拒绝
     */
    public void passOrNot(boolean pass) throws RuntimeException {
        if(pass) {
            if(this.status == -1) {
                this.status = 2;
            } else if(this.status == -2) {
                this.status = 0;
            } else if(this.status == -3) {
                this.status = 3;
            } else {
                throw new RuntimeException("Incorrect Device Status: " + this.status);
            }
        } else {
            if(this.status == -1) {
                this.status = 0;
            } else if(this.status == -2) {
                this.status = 2;
            } else if(this.status == -3) {
                this.status = 0;
            } else {
                throw new RuntimeException("Incorrect Device Status: " + this.status);
            }
        }
    }
}
