package com.nwpu.hass.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * <h3>hass</h3>
 * <p></p>
 *
 * @author : Qin
 * @date : 2021-01-08 03:31
 **/
@Entity
@Table(name = "warranty", schema = "hass", catalog = "")
public class WarrantyEntity {
    private long warrantyId;
    private Timestamp applyTime;
    private long device;
    private Long applyPerson;
    private Timestamp record;
    private String reason;
    private DeviceEntity deviceByDevice;
    private UserEntity userByApplyPerson;

    @Id
    @Column(name = "warrantyId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getWarrantyId() {
        return warrantyId;
    }

    public void setWarrantyId(long warrantyId) {
        this.warrantyId = warrantyId;
    }

    @Basic
    @Column(name = "applyTime", nullable = true)
    public Timestamp getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Timestamp applyTime) {
        this.applyTime = applyTime;
    }

    @Basic
    @Column(name = "device", nullable = false)
    public long getDevice() {
        return device;
    }

    public void setDevice(long device) {
        this.device = device;
    }

    @Basic
    @Column(name = "applyPerson", nullable = true)
    public Long getApplyPerson() {
        return applyPerson;
    }

    public void setApplyPerson(Long applyPerson) {
        this.applyPerson = applyPerson;
    }

    @Basic
    @Column(name = "record", nullable = false)
    public Timestamp getRecord() {
        return record;
    }

    public void setRecord(Timestamp record) {
        this.record = record;
    }

    @Basic
    @Column(name = "reason", nullable = true, length = -1)
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WarrantyEntity that = (WarrantyEntity) o;
        return warrantyId == that.warrantyId &&
                device == that.device &&
                Objects.equals(applyTime, that.applyTime) &&
                Objects.equals(applyPerson, that.applyPerson) &&
                Objects.equals(record, that.record) &&
                Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(warrantyId, applyTime, device, applyPerson, record, reason);
    }

    @ManyToOne
    @JoinColumn(name = "device", referencedColumnName = "deviceId", nullable = false,insertable = false,updatable = false)
    public DeviceEntity getDeviceByDevice() {
        return deviceByDevice;
    }

    public void setDeviceByDevice(DeviceEntity deviceByDevice) {
        this.deviceByDevice = deviceByDevice;
    }

    @ManyToOne
    @JoinColumn(name = "applyPerson", referencedColumnName = "userid",nullable = false,insertable = false,updatable = false)
    public UserEntity getUserByApplyPerson() {
        return userByApplyPerson;
    }

    public void setUserByApplyPerson(UserEntity userByApplyPerson) {
        this.userByApplyPerson = userByApplyPerson;
    }
}
