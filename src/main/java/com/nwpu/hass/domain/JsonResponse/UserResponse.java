package com.nwpu.hass.domain.JsonResponse;


import com.nwpu.hass.domain.UserEntity;
import lombok.Data;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-01 23:17:24
 */
@Data
public class UserResponse {
    private int mrole;
    private String email;

    public UserResponse(UserEntity userEntity) {
        this.mrole = userEntity.getMrole();
        this.email = userEntity.getEmail();
    }
}
