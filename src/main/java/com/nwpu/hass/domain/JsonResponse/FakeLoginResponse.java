package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;

/**
 * <h3>hass</h3>
 * <p>后台登录虚假接口</p>
 *
 * @author : Qin
 * @date : 2021-01-14 09:58
 **/
@Data
public class FakeLoginResponse {
    private String roles;
    private String introduction;
    private String avatar;
    private String name;

    public FakeLoginResponse(String roles, String introduction, String avatar, String name) {
        this.roles = roles;
        this.introduction = introduction;
        this.avatar = avatar;
        this.name = name;
    }
}
