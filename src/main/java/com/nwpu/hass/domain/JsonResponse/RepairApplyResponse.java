package com.nwpu.hass.domain.JsonResponse;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.UserEntity;
import lombok.Data;

import java.sql.Timestamp;
@Data
public class RepairApplyResponse extends ApplyEntity{

    private String repairReason;

    public RepairApplyResponse(UserEntity applicant, Timestamp applyTime,  DeviceEntity device,Long recordId, String repairReason) {

        super(applicant, applyTime, device,recordId);
        this.repairReason = repairReason;
    }
}
