package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;

/**
 * <h3>hass</h3>
 * <p>获取不同状态设备的数量的响应</p>
 *
 * @author : Qin
 * @date : 2021-01-12 14:24
 **/
@Data
public class DevicesByStatusResponse {
    private String status;

    private long count;

    public DevicesByStatusResponse(String status, long count) {
        this.status = status;
        this.count = count;
    }
}
