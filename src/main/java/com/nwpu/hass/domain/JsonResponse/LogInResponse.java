package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;

@Data
public class LogInResponse {
    private String token;
    private boolean role;

    public LogInResponse(String token, boolean role) {
        this.token = token;
        this.role = role;
    }
}
