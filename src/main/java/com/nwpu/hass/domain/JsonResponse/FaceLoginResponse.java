package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;

/**
 * <h3>hass</h3>
 * <p>人脸识别登录的返回结果</p>
 *
 * @author : Qin
 * @date : 2021-01-12 20:14
 **/
@Data
public class FaceLoginResponse {
    private String username;
    private String token;
    private boolean role;

    public FaceLoginResponse(String username,String token, boolean role) {
        this.username = username;
        this.token = token;
        this.role = role;
    }
}
