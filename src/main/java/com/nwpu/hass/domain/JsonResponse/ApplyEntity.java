package com.nwpu.hass.domain.JsonResponse;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.UserEntity;
import lombok.Data;

import java.sql.Timestamp;
@Data
public class ApplyEntity {

    UserEntity applicant;
    DeviceEntity device;
    Timestamp applyTime;
    Long recordId;

    public ApplyEntity(UserEntity applicant, Timestamp applyTime, DeviceEntity device,Long recordId) {

        this.applicant = applicant;
        this.applyTime = applyTime;
        this.device = device;
        this.recordId = recordId;
    }

    public ApplyEntity() {

    }
}

