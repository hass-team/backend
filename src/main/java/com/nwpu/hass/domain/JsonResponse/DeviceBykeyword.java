package com.nwpu.hass.domain.JsonResponse;

import com.nwpu.hass.domain.View4KeywordEntity;
import lombok.Data;
import org.apache.logging.log4j.core.impl.ReusableLogEventFactory;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-12 13:22:28
 */
@Data
public class DeviceBykeyword implements Comparable<DeviceBykeyword>{
    private String type;
    private String family;
    private String deviceName;
    private long count;
    private double score;

    public DeviceBykeyword(String type, String family, String deviceName, long count,double score) {
        this.type = type;
        this.family = family;
        this.deviceName = deviceName;
        this.count = count;
        this.score = score;
    }

    public static DeviceBykeyword view2entity(IdleDevicesGroupResponse view4KeywordEntity,double score){
        return new DeviceBykeyword(view4KeywordEntity.getType()
                ,view4KeywordEntity.getFamily()
                ,view4KeywordEntity.getDeviceName()
                ,view4KeywordEntity.getCount()
                ,score);
    }
    @Override
    public int compareTo(DeviceBykeyword deviceBykeyword) {
        return (int) ((deviceBykeyword.getScore() - this.getScore())*100);
    }
}
