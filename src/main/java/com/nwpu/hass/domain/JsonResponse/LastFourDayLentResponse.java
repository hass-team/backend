package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3>hass</h3>
 * <p>最近四天借出设备的列表</p>
 *
 * @author : Qin
 * @date : 2021-01-12 11:35
 **/
@Data
public class LastFourDayLentResponse {
    private String date;
    private long count;

    public LastFourDayLentResponse(String date, long count) {
        this.date = date;
        this.count = count;
    }

}
