package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;

/**
 * <h3>hass</h3>
 * <p>空闲设备列表</p>
 *
 * @author : Qin
 * @date : 2021-01-08 11:56
 **/
@Data
public class IdleDevicesGroupResponse {
    private String type;

    private String family;

    private String deviceName;

    private int count;

    private String url;


}
