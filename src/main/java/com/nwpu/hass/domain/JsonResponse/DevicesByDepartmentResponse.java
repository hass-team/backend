package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;

/**
 * <h3>hass</h3>
 * <p>通过设备部门获得设备分组</p>
 *
 * @author : Qin
 * @date : 2021-01-12 15:17
 **/
@Data
public class DevicesByDepartmentResponse {
    private String departmentName;

    private Long devidceCount;

    public DevicesByDepartmentResponse(String departmentName, Long devidceCount) {
        this.departmentName = departmentName;
        this.devidceCount = devidceCount;
    }
}
