package com.nwpu.hass.domain.JsonResponse;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-11 21:43:12
 */
@Data
@Slf4j
public class UsedDeviceFreqResponse implements Comparable<UsedDeviceFreqResponse> {

    private Long count;
    private String deviceName;

    public static List<UsedDeviceFreqResponse> res2List(List result){

        List<UsedDeviceFreqResponse> responses = new ArrayList<>();
        if (result != null) {
            for (Object obj : result) {
                UsedDeviceFreqResponse res = new UsedDeviceFreqResponse();
                Object[] row = (Object[]) obj;
                res.setDeviceName(row[0].toString());
                res.setCount(Long.parseLong(row[1].toString()));
                responses.add(res);
            }
        }
        return responses;
    }

    @Override
    public int compareTo(UsedDeviceFreqResponse user) {
        return (int) (user.getCount() - this.getCount());
    }
}
