package com.nwpu.hass.domain.str2json.facematchjson;

import java.util.*;

/**
 * <h3>hass</h3>
 * <p>json中的result字段</p>
 *
 * @author : Qin
 * @date : 2021-01-10 22:03
 **/
public class Result {
    private String face_token;
    private List<User_list> user_list;
    public void setFace_token(String face_token) {
        this.face_token = face_token;
    }
    public String getFace_token() {
        return face_token;
    }

    public void setUser_list(List<User_list> user_list) {
        this.user_list = user_list;
    }
    public List<User_list> getUser_list() {
        return user_list;
    }
}
