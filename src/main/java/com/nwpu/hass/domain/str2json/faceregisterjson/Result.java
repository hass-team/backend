package com.nwpu.hass.domain.str2json.faceregisterjson;

/**
 * <h3>hass</h3>
 * <p>人脸采集信息结果</p>
 *
 * @author : Qin
 * @date : 2021-01-12 17:02
 **/
public class Result {
    private String face_token;
    private Location location;
    public void setFace_token(String face_token) {
        this.face_token = face_token;
    }
    public String getFace_token() {
        return face_token;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
    public Location getLocation() {
        return location;
    }

}
