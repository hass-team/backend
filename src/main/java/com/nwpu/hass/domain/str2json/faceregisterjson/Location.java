package com.nwpu.hass.domain.str2json.faceregisterjson;

/**
 * <h3>hass</h3>
 * <p>人脸位置信息</p>
 *
 * @author : Qin
 * @date : 2021-01-12 17:03
 **/
public class Location {
    private double left;
    private double top;
    private int width;
    private int height;
    private int rotation;
    public void setLeft(double left) {
        this.left = left;
    }
    public double getLeft() {
        return left;
    }

    public void setTop(double top) {
        this.top = top;
    }
    public double getTop() {
        return top;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    public int getHeight() {
        return height;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }
    public int getRotation() {
        return rotation;
    }

}
