package com.nwpu.hass.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "systemMessage", schema = "hass", catalog = "")
public class SystemMessageEntity {
    private long messageId;
    private String content;
    private Timestamp record;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "messageId", nullable = false)
    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    @Basic
    @Column(name = "content", nullable = true, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "record", nullable = true)
    public Timestamp getRecord() {
        return record;
    }

    public void setRecord(Timestamp record) {
        this.record = record;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemMessageEntity that = (SystemMessageEntity) o;
        return messageId == that.messageId && Objects.equals(content, that.content) && Objects.equals(record, that.record);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageId, content, record);
    }
}
