package com.nwpu.hass.domain;

import com.auth0.jwt.interfaces.Claim;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @author alecHe
 * @desc 存储在Redis中的payload
 * @date 2021-01-09 10:53:35
 */
@Data
@Slf4j
public class AuthEntity {
    private Long userId;
    private int role;
    private String clientCode;

    public AuthEntity(long userId, int role, String clientCode) {
        this.clientCode = clientCode;
        this.role = role;
        this.userId = userId;
    }

    public static AuthEntity Map2Info(Map<String, Claim> map){
        Long userId = map.get("userId").asLong();
        Integer role = map.get("role").asInt();

        return new AuthEntity(map.get("userId").asLong(),
                map.get("role").asInt(),
                map.get("clientCode").asString());
    }
}
