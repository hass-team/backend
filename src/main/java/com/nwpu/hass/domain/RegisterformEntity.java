package com.nwpu.hass.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * <h3>hass</h3>
 * <p></p>
 *
 * @author : Qin
 * @date : 2021-01-08 03:31
 **/
@Entity
@Table(name = "registerform", schema = "hass")
public class RegisterformEntity {
    private long registerid;
    private Timestamp borrowTime;
    private Timestamp returnTime;
    private long device;
    private long borrowPerson;
    private Timestamp record;
    private DeviceEntity deviceByDevice;
    private UserEntity userByBorrowPerson;

    @Id
    @Column(name = "registerid", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getRegisterid() {
        return registerid;
    }

    public void setRegisterid(long registerid) {
        this.registerid = registerid;
    }

    @Basic
    @Column(name = "borrowTime", nullable = true)
    public Timestamp getBorrowTime() {
        return borrowTime;
    }

    public void setBorrowTime(Timestamp borrowTime) {
        this.borrowTime = borrowTime;
    }

    @Basic
    @Column(name = "returnTime", nullable = true)
    public Timestamp getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Timestamp returnTime) {
        this.returnTime = returnTime;
    }

    @Basic
    @Column(name = "device", nullable = false)
    public long getDevice() {
        return device;
    }

    public void setDevice(long device) {
        this.device = device;
    }

    @Basic
    @Column(name = "borrowPerson", nullable = false)
    public long getBorrowPerson() {
        return borrowPerson;
    }

    public void setBorrowPerson(long borrowPerson) {
        this.borrowPerson = borrowPerson;
    }

    @Basic
    @Column(name = "record", nullable = false)
    public Timestamp getRecord() {
        return record;
    }

    public void setRecord(Timestamp record) {
        this.record = record;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterformEntity that = (RegisterformEntity) o;
        return registerid == that.registerid &&
                device == that.device &&
                borrowPerson == that.borrowPerson &&
                Objects.equals(borrowTime, that.borrowTime) &&
                Objects.equals(returnTime, that.returnTime) &&
                Objects.equals(record, that.record);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registerid, borrowTime, returnTime, device, borrowPerson, record);
    }

    @ManyToOne
    @JoinColumn(name = "device", referencedColumnName = "deviceId", nullable = false,insertable = false,updatable = false)
    public DeviceEntity getDeviceByDevice() {
        return deviceByDevice;
    }

    public void setDeviceByDevice(DeviceEntity deviceByDevice) {
        this.deviceByDevice = deviceByDevice;
    }

    @ManyToOne
    @JoinColumn(name = "borrowPerson", referencedColumnName = "userid", nullable = false,insertable = false,updatable = false)
    public UserEntity getUserByBorrowPerson() {
        return userByBorrowPerson;
    }

    public void setUserByBorrowPerson(UserEntity userByBorrowPerson) {
        this.userByBorrowPerson = userByBorrowPerson;
    }
}
