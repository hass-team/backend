package com.nwpu.hass.domain;

import lombok.Data;

/**
 * @author alecHe
 * @desc 微信接收的Token模板
 * @date 2021-01-13 15:29:41
 */
@Data
public class TokenBody {
    private String access_token;
    private String expires_in;
}
