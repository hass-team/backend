package com.nwpu.hass.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

/**
 * <h3>hass</h3>
 * <p></p>
 *
 * @author : Qin
 * @date : 2021-01-08 03:31
 **/
@Entity
@Table(name = "user", schema = "hass", catalog = "")
public class UserEntity {
    private long userid;
    private int mrole;
    private String username;
    private String mpassword;
    private String fullname;
    private String depwork;
    private String email;
    private String phone;
    private String userAuthpic;
    private String wechatAccount;
    private Byte isDelete;
    private Timestamp record;
    private Collection<RegisterformEntity> registerformsByUserid;
    private Collection<WarrantyEntity> warrantiesByUserid;
    private Collection<MessageEntity> messagesByUserid;

    @Id
    @Column(name = "userid", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "mrole", nullable = false)
    public int getMrole() {
        return mrole;
    }

    public void setMrole(int mrole) {
        this.mrole = mrole;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 255)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "mpassword", nullable = false, length = 255)
    public String getMpassword() {
        return mpassword;
    }

    public void setMpassword(String mpassword) {
        this.mpassword = mpassword;
    }

    @Basic
    @Column(name = "fullname", nullable = true, length = 255)
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Basic
    @Column(name = "depwork", nullable = true, length = 255)
    public String getDepwork() {
        return depwork;
    }

    public void setDepwork(String depwork) {
        this.depwork = depwork;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "phone", nullable = false, length = 11)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "userAuthpic", nullable = true, length = 255)
    public String getUserAuthpic() {
        return userAuthpic;
    }

    public void setUserAuthpic(String userAuthpic) {
        this.userAuthpic = userAuthpic;
    }

    @Basic
    @Column(name = "wechatAccount", nullable = true, length = 255)
    public String getWechatAccount() {
        return wechatAccount;
    }

    public void setWechatAccount(String wechatAccount) {
        this.wechatAccount = wechatAccount;
    }

    @Basic
    @Column(name = "isDelete", nullable = true)
    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }

    @Basic
    @Column(name = "record", nullable = false)
    public Timestamp getRecord() {
        return record;
    }

    public void setRecord(Timestamp record) {
        this.record = record;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return userid == that.userid &&
                mrole == that.mrole &&
                Objects.equals(username, that.username) &&
                Objects.equals(mpassword, that.mpassword) &&
                Objects.equals(fullname, that.fullname) &&
                Objects.equals(depwork, that.depwork) &&
                Objects.equals(email, that.email) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(userAuthpic, that.userAuthpic) &&
                Objects.equals(wechatAccount, that.wechatAccount) &&
                Objects.equals(isDelete, that.isDelete) &&
                Objects.equals(record, that.record);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userid, mrole, username, mpassword, fullname, depwork, email, phone, userAuthpic, wechatAccount, isDelete, record);
    }

    @OneToMany(mappedBy = "userByBorrowPerson")
    @JsonIgnore
    public Collection<RegisterformEntity> getRegisterformsByUserid() {
        return registerformsByUserid;
    }

    public void setRegisterformsByUserid(Collection<RegisterformEntity> registerformsByUserid) {
        this.registerformsByUserid = registerformsByUserid;
    }

    @OneToMany(mappedBy = "userByApplyPerson")
    @JsonIgnore
    public Collection<WarrantyEntity> getWarrantiesByUserid() {
        return warrantiesByUserid;
    }

    public void setWarrantiesByUserid(Collection<WarrantyEntity> warrantiesByUserid) {
        this.warrantiesByUserid = warrantiesByUserid;
    }

    @OneToMany(mappedBy = "userByUserid")
    @JsonIgnore
    public Collection<MessageEntity> getMessagesByUserid() {
        return messagesByUserid;
    }

    public void setMessagesByUserid(Collection<MessageEntity> messagesByUserid) {
        this.messagesByUserid = messagesByUserid;
    }
}
