package com.nwpu.hass.finals;

import java.util.*;

/**
 * <h3>hass</h3>
 * <p>设备状态和描述的对应关系</p>
 *
 * @author : Qin
 * @date : 2021-01-12 14:35
 **/
public class FinalData {
    public static final Map<Integer, String> STATUS_MAP = new LinkedHashMap<>();

    public static final Set<String> DEPARTMENTS = new HashSet<>();

    static {
        STATUS_MAP.put(-5,"入库申请");
        STATUS_MAP.put(-4,"报废申请");
        STATUS_MAP.put(-3,"报修申请");
        STATUS_MAP.put(-1,"借出申请");
        STATUS_MAP.put(-2,"归还申请");
        STATUS_MAP.put(3,"报修");
        STATUS_MAP.put(4,"报废");
        STATUS_MAP.put(2,"占用");
        STATUS_MAP.put(0,"空闲");

        DEPARTMENTS.add("仓库");
        DEPARTMENTS.add("管理学院");
        DEPARTMENTS.add("理学院");
        DEPARTMENTS.add("动能学院");
        DEPARTMENTS.add("生命学院");
        DEPARTMENTS.add("未入库");

    }
}
