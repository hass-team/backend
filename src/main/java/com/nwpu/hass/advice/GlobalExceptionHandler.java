package com.nwpu.hass.advice;


import com.nwpu.hass.exceptions.AuthorizeException;
import com.nwpu.hass.exceptions.AuthorizeWechatException;
import com.nwpu.hass.utils.Code;
import com.nwpu.hass.utils.MyResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.rmi.RemoteException;

@ControllerAdvice
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.OK)
	public MyResponseEntity<Object> handleGlobalExceptions(Exception e) {
		e.printStackTrace();
		return new MyResponseEntity<>(Code.ERROR, e.getMessage());
	}

	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.OK)
	public MyResponseEntity<Object> handleMissingParamExceptions(MissingServletRequestParameterException e) {
		log.error(e.getMessage());
		return new MyResponseEntity<>(Code.BAD_REQUEST, "缺少参数");
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(HttpStatus.OK)
	public MyResponseEntity<Object> handleMisParamTypeExceptions(MethodArgumentTypeMismatchException e) {
		log.error(e.getMessage());
		return new MyResponseEntity<>(Code.BAD_REQUEST, "参数类型错误");
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.OK)
	public MyResponseEntity<Object> MethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
		// 从异常对象中拿到ObjectError对象
		ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
		// 然后提取错误提示信息进行返回
		return new MyResponseEntity<>(Code.BAD_REQUEST, objectError.getDefaultMessage());
	}
	@ExceptionHandler(AuthorizeException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public MyResponseEntity<Object> AuthorizeExceptionHandler(AuthorizeException e) {
		//告诉前端，token失效，重新登录。data里面包含的即为验证的地址 => 登录端口
		return new MyResponseEntity<>(Code.INVALID_TOKEN,"/verifyCenter/login");
	}

	@ExceptionHandler(RemoteException.class)
	@ResponseStatus(HttpStatus.OK)
	public MyResponseEntity<Object> RemoteExceptionHandler(RemoteException e) {
		//告诉前端，token失效，重新登录。data里面包含的即为验证的地址 => 登录端口
		return new MyResponseEntity<>(Code.BAD_REQUEST,"计算端口未打开",null);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.OK)
	public MyResponseEntity<Object> DataIntegrityViolationExceptionHandler(DataIntegrityViolationException e) {
		//微信的openID设置为unique，不能重复。所以会报此错误
		return new MyResponseEntity<>(Code.BAD_REQUEST,"用户已注册",null);
	}

	@ExceptionHandler(AuthorizeWechatException.class)
	@ResponseStatus(HttpStatus.OK)
	public MyResponseEntity<Object> AuthorizeWechatExceptionHandler(AuthorizeWechatException e) {
		//微信的openID设置为unique，不能重复。所以会报此错误
		return new MyResponseEntity<>(Code.BAD_REQUEST,e.getMessage(),null);
	}

}
