package com.nwpu.hass.advice;


import com.nwpu.hass.controller.UserController;
import com.nwpu.hass.exceptions.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackageClasses = {UserController.class})
@ResponseBody
public class UserControllerAdvice {

//	@ExceptionHandler(ValidationException.class)
//	@ResponseStatus(HttpStatus.UNAUTHORIZED)
//	public MyResponseEntity<RegisterResponse> handleValidationException(ValidationException e) {
//		return new MyResponseEntity<>(Code.BAD_REQUEST, e.getMessage(), null);
//	}

}
