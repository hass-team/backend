package com.nwpu.hass.controller;

import com.nwpu.hass.domain.MessageEntity;
import com.nwpu.hass.domain.SystemMessageEntity;
import com.nwpu.hass.service.MessageService;
import com.nwpu.hass.service.RedisService;
import com.nwpu.hass.utils.Code;
import com.nwpu.hass.utils.MyResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * 消息通知
 */
@Slf4j
@RestController
@RequestMapping(value = "/message")
public class MessageController {

    @Resource
    MessageService messageService;
    @Autowired
    RedisService redisService;

    /**
     * 用户请求获取自己的消息列表
     */
    @GetMapping(value = "/getMessageList/{pageNo}")
    MyResponseEntity<List<MessageEntity>> getMessageList(@RequestHeader("token") String token,
                                                         @PathVariable("pageNo") @Valid long pageNo) {

        Long userId = redisService.getUserId(token);
        List<MessageEntity> messageEntities = messageService.getMessageList(userId, pageNo);
        if(Objects.isNull(messageEntities) || messageEntities.isEmpty()) {
            return new MyResponseEntity<>(Code.ERROR, "没有更多消息", null);
        }
        log.info("用户请求获取自己的消息列表");
        return new MyResponseEntity<>(messageEntities);
    }

    /**
     * 管理员请求获取系统消息列表
     */
    @GetMapping(value = "/getSysMessageList/{pageNo}")
    MyResponseEntity<List<SystemMessageEntity>> getSysMessageList(@RequestHeader("token") String token,
                                                         @PathVariable long pageNo) {

        Long userId = redisService.getUserId(token);
        List<SystemMessageEntity> systemMessageEntities = messageService.getSystemMessageList(pageNo);
        if(Objects.isNull(systemMessageEntities) || systemMessageEntities.isEmpty()) {
            return new MyResponseEntity<>(Code.ERROR, "没有更多消息", null);
        }
        log.info("管理员请求获取系统消息列表");
        return new MyResponseEntity<>(systemMessageEntities);
    }
}
