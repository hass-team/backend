package com.nwpu.hass.controller;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.JsonResponse.UsedDeviceFreqResponse;
import com.nwpu.hass.service.DeviceService;
import com.nwpu.hass.service.RegisterformService;
import com.nwpu.hass.serviceimpl.DeviceServiceImpl;
import com.nwpu.hass.utils.MyResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-11 21:21:46
 */
@Slf4j
@RestController
@RequestMapping("data")
public class DataController {
    @Resource
    private RegisterformService registerformService;

    @Resource
    private DeviceService deviceService;

    @GetMapping("used")
    public MyResponseEntity<Object> getMostUsedDevices(){
        List<UsedDeviceFreqResponse> devices = registerformService.getDeviceByFreq();
        List<UsedDeviceFreqResponse> res;
        if (devices.size() > 10){
            Collections.sort(devices);
            res = devices.subList(0,9);
        }
        else{
            res = devices;
        }
        log.info("已获取使用频率最多的设备");
        return new MyResponseEntity<>(res);
    }

    @GetMapping("lend")
    public MyResponseEntity<Object> getLastFourDayLentDevice(){
        return new MyResponseEntity<>(registerformService.getLastFourLentDevice());
    }

    @GetMapping("status")
    public MyResponseEntity<Object> getDevicesByStatus(){
        return new MyResponseEntity<>(deviceService.getDeviceGroupByStatus());
    }

    @GetMapping("department")
    public MyResponseEntity<Object> getDevicesByDepartment(){
        return new MyResponseEntity<>(deviceService.getDeviceGroupByDepartment());
    }
}
