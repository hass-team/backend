package com.nwpu.hass.controller;

import com.nwpu.hass.domain.DeviceEntity;
import com.nwpu.hass.domain.JsonResponse.IdleDevicesGroupResponse;
import com.nwpu.hass.repository.DeviceRepository;
import com.nwpu.hass.service.DeviceService;
import com.nwpu.hass.serviceimpl.ComputeServiceImpl;
import com.nwpu.hass.utils.Code;
import com.nwpu.hass.utils.MyResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.relational.core.sql.In;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;


/**
 * @author alecHe
 * @desc ...
 * @date 2021-01-11 19:23:26
 */
@Slf4j
@RestController
@RequestMapping("search")
public class SearchController {

    @Resource
    private ComputeServiceImpl computeService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private DeviceRepository deviceRepository;

    @GetMapping("/keyword")
    public MyResponseEntity<Object> searchByKeyword(@RequestParam("keyword")String keyword) throws Exception {
        if(StringUtils.isBlank(keyword)){
            log.warn("关键字为空");
            return new MyResponseEntity<>(Code.BAD_OPERATION,"关键字为空");
        }
        log.info("已获取匹配上关键的设备列表");
        List<IdleDevicesGroupResponse> list = deviceService.getAllIdleDeviceGroup(0, Integer.MAX_VALUE);

        return new MyResponseEntity<>(computeService.findByKeyword(keyword,list));
    }

    @GetMapping("/family")
    public MyResponseEntity<Object> searchByType(@RequestParam("family")String family){
        if(StringUtils.isBlank(family)){
            return new MyResponseEntity<>(Code.BAD_OPERATION,"设备中为空");
        }
        log.info("通过设备种类筛选成功，" + "设备类别当前为" + family);
        return new MyResponseEntity<>(deviceService.getDevicesByFamily(family));
    }

    @GetMapping("/status")
    public MyResponseEntity<Object> searchByStatus(@RequestParam("status")int status){
        log.info("通过设备种类筛选成功，" + "设备类别当前为" + status);
        return new MyResponseEntity<>(deviceService.getDevicesByStatus(status));
    }
}
