package com.nwpu.hass.controller;

import com.nwpu.hass.domain.JsonRequest.ApplyForRepairRequest;
import com.nwpu.hass.domain.JsonRequest.ProcessApplyRequest;
import com.nwpu.hass.domain.JsonResponse.RepairApplyResponse;
import com.nwpu.hass.domain.WarrantyEntity;
import com.nwpu.hass.service.RedisService;
import com.nwpu.hass.service.WarrantyService;
import com.nwpu.hass.utils.Code;
import com.nwpu.hass.utils.MyResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.*;
import java.util.Objects;

/**
 * 设备报修Controller
 *
 * @author wjy
 * @version v1.0.0
 */
@Slf4j
@RestController
@RequestMapping(value = "/device/repair")
public class WarrantyController {

    @Resource
    private WarrantyService warrantyService;

    @Autowired
    private RedisService redisService;

    /**
     * 提交报修申请
     */
    @PostMapping("/append")
    public MyResponseEntity<Object> appendRequire(@RequestHeader("token") String token,@RequestBody @Valid ApplyForRepairRequest applyForRepairRequest) {
        Long deviceId = applyForRepairRequest.getDeviceId();
        String repairReason = applyForRepairRequest.getRepairReason();
        long userid = redisService.getUserId(token);
        String msg = warrantyService.appendRepair(deviceId, repairReason, userid);
        if(msg.equals("OK")){
            log.info("报修申请已提交");
            return new MyResponseEntity<>(null);
        }else{
            return new MyResponseEntity<>(Code.ERROR,msg,null);
        }

    }

    /**
     * 查看一条报修申请
     */
    @GetMapping("/{id}")
    public MyResponseEntity<RepairApplyResponse> getRepairRecord(@PathVariable long id) {
        RepairApplyResponse repairApplyResponse = warrantyService.getOneRepairRecord(id);
        if(Objects.isNull(repairApplyResponse)) {
            log.info("报修申请信息已获取");
            return new MyResponseEntity<>(Code.OK,repairApplyResponse);
        }
        return new MyResponseEntity<>(Code.ERROR, "记录id不存在", null);
    }

    /**
     * 处理一条报修申请
     */
    @PostMapping("/")
    public MyResponseEntity<Object> processRepairRecord(@RequestBody @Valid ProcessApplyRequest request) {

        boolean pass = request.isPass();
        long recordId = request.getId();
        String msg = warrantyService.checkOneById(recordId, pass);
        if(msg.equals("OK")){
            log.info("已处理一条报修申请");
            return new MyResponseEntity<>(Code.OK,msg,null);
        }else{
            return new MyResponseEntity<>(Code.ERROR,msg,null);
        }
    }

    /**
     * 查看报修申请列表
     */
    @GetMapping("/")
    public MyResponseEntity<List<RepairApplyResponse>> applyList(@RequestParam("pageNo") Integer pageNo){
        List<RepairApplyResponse> responses = warrantyService.getCheckList(pageNo);
        log.info("报修申请列表已获取");
        return new MyResponseEntity<>(Code.OK,responses);
    }
}
