package com.nwpu.hass.utils;


import com.nwpu.hass.service.ComputeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author alecHe
 * @desc 一些自定义的小工具可以写在这个类里面
 * @date 2020-12-08 13:06:36
 */
@Slf4j
public class Tools {
    public static boolean isEmptyString(String s){
        return s==null || s.isEmpty();
    }
    public static Timestamp timestamp(){
        return new Timestamp(System.currentTimeMillis());
    }
    public static String translator(String msg){
        return null;
    }

    public static Map<String, String> getQueryParameters() {

        Map<String, String> map = new HashMap<>();

        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();

        // 获取请求头
        Enumeration<String> enumeration = request.getHeaderNames();
        StringBuffer headers = new StringBuffer();
        while (enumeration.hasMoreElements()) {
            String name = enumeration.nextElement();
            String value = request.getHeader(name);
            map.put(name,value);
        }
        return map;
    }
}
