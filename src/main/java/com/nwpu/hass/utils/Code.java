package com.nwpu.hass.utils;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author HeAlec
 * @desc Status code
 * @date 2020-12-08 16:55:09
 */
public enum Code {
	OK(1,"操作成功"),
	BAD_OPERATION(0,"参数校验失败"),
	BAD_REQUEST(-1,"响应失败"),
	INVALID_TOKEN(-2,"未授权操作"),
	ERROR(5, "未知错误"),
	ADMIN_OK(20000,"操作成功");

	private final int code;
	private final String msg;

	Code(int code,String msg) {
		this.code = code;
		this.msg = msg;
	}

	@JsonValue
	public int getCode() {
		return this.code;
	}
	@JsonValue
	public String getMsg(){
		return this.msg;
	}
}
