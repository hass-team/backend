package com.nwpu.hass.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Project: bsss<br>
 * Created: 3:36 PM, 11/12/2020<br>
 *
 * @author Zejia Lin
 * @version 0.0.1
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MyResponseEntity<T> {

	protected int code;
	protected String msg;
	protected T data;

	public MyResponseEntity(T data) {
		this(Code.OK, data);
	}

	public MyResponseEntity(Code code, T data) {
		this.code = code.getCode();
		this.msg = code.getMsg();
		this.data = data;
	}
	public MyResponseEntity(Code code, String msg, T data) {
		this.code = code.getCode();
		this.msg = msg;
		this.data = data;
	}

}
