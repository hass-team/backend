package com.nwpu.hass.utils;

import com.nwpu.hass.exceptions.NoFileFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileExistsException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Slf4j
@Component
@PropertySource("classpath:application.yml")
public class FileComponent {

    @Value("${filepath}")
    private String filepath;

    private static final String SPLIT = "/";

    public boolean checkExist(String filename,long userId){
        File file = new File(filepath + userId + "_" + filename);
        return file.exists();
    }

    public void uploadFile(MultipartFile file, long userId) throws IOException {
        uploadFile(file, "", userId);
    }

    public void uploadFile(MultipartFile file, String subPath, long userId) throws IOException {

        String targetPath = filepath + SPLIT + subPath + SPLIT;
        File targetFile = new File(targetPath);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }

        String filename = userId + "_" + file.getOriginalFilename();

        FileOutputStream out = new FileOutputStream(targetPath + filename);
        out.write(file.getBytes());

        out.close();

    }

    public File createFile(String fileName, String fileType) throws IOException {
        return createFile("", fileName, fileType);
    }

    public File createFile(String subPath, String fileName, String fileType) throws IOException {

        String targetPath = filepath + SPLIT + subPath;
        log.info(targetPath);
        File targetFile = new File(targetPath);
        log.info("****" + targetFile.getAbsolutePath());
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        File newFile = new File(targetPath + fileName +"." + fileType);
        log.info(newFile.getAbsolutePath());
        return newFile;

    }
    public void downloadFile(HttpServletResponse response,String filename) throws IOException {

        File file = new File(filepath + "/" + filename);

        if(file.exists()){
            response.setContentType("application/octet-stream");
            response.setHeader("content-type", "application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(filename, StandardCharsets.UTF_8));
            byte[] buffer = new byte[1024];
            //输出流
            OutputStream os = null;
            FileInputStream fis= new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis);
            os = response.getOutputStream();
            int i = bis.read(buffer);
            while(i != -1){
                os.write(buffer);
                i = bis.read(buffer);
            }
        }
        else{
            throw new NoFileFoundException("NO FILE FOUND");
        }

    }
}
