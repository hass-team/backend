package com.nwpu.hass.utils;

public enum Status {
    FREE(0,"空闲"),
    ONBORROW(-1,"借出申请状态"),
    BUSY(2,"占用"),
    ONRETURN(-2,"归还申请状态"),
    ONREPAIR(-3, "保修申请状态"),
    REPAIRING(3,"保修"),
    DEAD(4,"报废");

    private final int status;
    private final String desc;

    Status(int status,String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus(){
        return status;
    }

    public String getDesc(){
        return desc;
    }
}
