package com.nwpu.hass.config;

import com.nwpu.hass.interceptor.AuthInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <h3>hass</h3>
 * <p>配置Redis拦截器</p>
 *
 * @author : Qin
 * @date : 2021-01-07 11:08
 **/
@Configuration
public class AuthConfig implements WebMvcConfigurer {
    @Autowired AuthInterceptor authInterceptor;
//    @Bean
//    public AuthInterceptor initAuthInterceptor(){
//        return new AuthInterceptor();
//    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 为了方便测试接口，暂时没有设置拦截路径
        registry.addInterceptor(authInterceptor);
    }
}
